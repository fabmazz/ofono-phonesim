Name:    ofono-phonesim
Summary: Phone simulator for modem testing
Version: 1.21
Release: 1%{?dist}
License: GPLv2
URL:     https://git.kernel.org/pub/scm/network/ofono/phonesim.git/
Source0: https://git.kernel.org/pub/scm/network/ofono/phonesim.git/snapshot/phonesim-%{version}.tar.gz
BuildRequires: automake
BuildRequires: gcc-c++
BuildRequires: qt-devel
Requires: qt
Requires: libpng
Requires: freetype
Requires: libSM
Requires: libICE
Requires: libXrender
Requires: fontconfig
Requires: libexttextcat
Requires: libX11
Requires: harfbuzz
Requires: libxcb
Requires: graphite2
Requires: libXau
Requires: libXdmcp

%description
A tool to simulate an phone for ofono.

%prep
%setup -q -n phonesim-%{version}

%build
./bootstrap-configure
make

%install
%make_install

%files
%doc ChangeLog AUTHORS README
%license COPYING
%{_bindir}/phonesim
%{_datadir}/phonesim/default.xml

%changelog
* Thu Apr 09 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 1.21-1
- Initial specfile 
